public class ObjectClass {

    private String lastName;
    private String firstName;
    private String city;
    private String street;
    private String plz;
    private String birthdate;


    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getPLZ() {
        return plz;
    }

    public void setPLZ(String plz) {
        this.plz = plz;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public  String getBirthDate(){ return birthdate; }

    public void setBirthdate(String birthdate){this.birthdate = birthdate; }



    public String returnAll() {
        return firstName + lastName + street + plz + city + birthdate;
    }
}
