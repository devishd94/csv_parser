import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {

        Scanner s = new Scanner(System.in);
        System.out.print("Enter the name of CSV File: ");
        String CSV_Name = s.nextLine();
        File file = new File(CSV_Name);

        List<ObjectClass> users = new ArrayList<>();
        ObjectClass newObject = new ObjectClass();

        try{
            Scanner inputStream = new Scanner(file);

            while(inputStream.hasNext()){

                inputStream.nextLine();

                String data = inputStream.next();
                String[] values = data.split(",");

                String strLastName = values[0];
                String strFirstName = values[1];
                String strStreet = values[2];
                String strPLZ = values[3];
                String strCity = values[4];
                String strBirthdate = values[5];

                newObject.setLastName(strLastName);
                newObject.setFirstName(strFirstName);
                newObject.setStreet(strStreet);
                newObject.setPLZ(strPLZ);
                newObject.setCity(strCity);
                newObject.setBirthdate(strBirthdate);
                users.add(newObject);

                System.out.println(newObject.returnAll());
                ageCalculator(values[5]);
                //inputStream.close();
            }

        } catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }



    public static void ageCalculator(String values){

        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {

            LocalDate today = LocalDate.now();
            Date date1 = formatter.parse(values);
            LocalDate date = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

            Period p = Period.between(date, today);

            System.out.println(p.getYears());

        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

}

